package com.nelyubin.improvetest.workwithphoto;

import android.net.Uri;
import android.os.Environment;
import java.io.File;

public final class GeneratorURI
{
    public static Uri generateUri()
    {
        final File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "ImproveTest");
        if (!directory.exists())
            directory.mkdirs();
        final File file = new File(directory.getPath() + "/" + "photo_"
                        + System.currentTimeMillis() + ".jpg");
        return Uri.fromFile(file);
    }
}

package com.nelyubin.improvetest.errorhandling;


import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.nelyubin.improvetest.R;

public final class PasswordInputErrors implements View.OnFocusChangeListener
{
    private TextInputLayout passwordTextInputLayout;
    private TextInputEditText passwordTextInputEditText;

    public PasswordInputErrors(TextInputLayout passwordTextInputLayout, TextInputEditText passwordTextInputEditText)
    {
        this.passwordTextInputLayout = passwordTextInputLayout;
        this.passwordTextInputEditText = passwordTextInputEditText;
    }
    @Override
    public void onFocusChange(View view, boolean b)
    {
        if(passwordTextInputEditText.getText().toString().isEmpty())
        {
            passwordTextInputLayout.setErrorEnabled(true);
            passwordTextInputLayout.setError(view.getContext().getString(R.string.string_error_password_is_empty));
        }
        else if(passwordTextInputEditText.getText().toString().length() < 6)
        {
            passwordTextInputLayout.setErrorEnabled(true);
            passwordTextInputLayout.setError(view.getContext().getString(R.string.string_error_password_length_less_six));
        }
        else if(!passwordTextInputEditText.getText().toString().matches(".*[A-Za-z]+.*"))
        {
            passwordTextInputLayout.setErrorEnabled(true);
            passwordTextInputLayout.setError(view.getContext().getString(R.string.string_error_password_must_contain_letter));
        }
        else if(!passwordTextInputEditText.getText().toString().matches(".*\\d+.*"))
        {
            passwordTextInputLayout.setErrorEnabled(true);
            passwordTextInputLayout.setError(view.getContext().getString(R.string.string_error_password_must_contain_digit));
        }
        else if(passwordTextInputEditText.getText().toString().matches(".*\\W+.*"))
        {
            passwordTextInputLayout.setErrorEnabled(true);
            passwordTextInputLayout.setError(view.getContext().getString(R.string.string_error_password_must_contain_only_letters_or_digits));
        }
        else
        {
            passwordTextInputLayout.setErrorEnabled(false);
        }
    }
}

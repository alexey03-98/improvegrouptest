package com.nelyubin.improvetest.errorhandling;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.nelyubin.improvetest.R;

public final class PhoneInputErrors implements View.OnFocusChangeListener
{
    private TextInputLayout phoneTextInputLayout;
    private TextInputEditText phoneTextInputEditText;

    public PhoneInputErrors(TextInputLayout phoneTextInputLayout, TextInputEditText phoneTextInputEditText)
    {
        this.phoneTextInputLayout = phoneTextInputLayout;
        this.phoneTextInputEditText = phoneTextInputEditText;
    }

    @Override
    public void onFocusChange(View view, boolean b)
    {
        final String pattern = "\\++?7?\\d{10}";
        if(phoneTextInputEditText.getText().toString().isEmpty())
        {
            phoneTextInputLayout.setErrorEnabled(true);
            phoneTextInputLayout.setError(view.getContext().getString(R.string.string_error_phone_is_empty));
        }
        else if(!phoneTextInputEditText.getText().toString().matches(pattern))
        {
            phoneTextInputLayout.setErrorEnabled(true);
            phoneTextInputLayout.setError(view.getContext().getString(R.string.string_error_phone_is_not_international_format));
        }
        else
        {
            phoneTextInputLayout.setErrorEnabled(false);
        }
    }
}

package com.nelyubin.improvetest.errorhandling;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import com.nelyubin.improvetest.R;

public final class EmailInputErrors implements View.OnFocusChangeListener
{
    private TextInputLayout emailTextInputLayout;
    private TextInputEditText emailTextInputEditText;

    public EmailInputErrors(TextInputLayout emailTextInputLayout, TextInputEditText emailTextInputEditText)
    {
        this.emailTextInputLayout = emailTextInputLayout;
        this.emailTextInputEditText = emailTextInputEditText;
    }

    @Override
    public void onFocusChange(View view, boolean b)
    {
        if(emailTextInputEditText.getText().toString().isEmpty())
        {
            emailTextInputLayout.setErrorEnabled(true);
            emailTextInputLayout.setError(view.getContext().getString(R.string.string_error_email_address_is_empty));
        }
        else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailTextInputEditText.getText().toString()).matches())
        {
            emailTextInputLayout.setErrorEnabled(true);
            emailTextInputLayout.setError(view.getContext().getString(R.string.string_error_email_address_is_not_valid));
        }
        else
        {
            emailTextInputLayout.setErrorEnabled(false);
        }
    }
}

package com.nelyubin.improvetest.listeners;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Toast;

import com.nelyubin.improvetest.SendDataActivity;
import com.nelyubin.improvetest.model.Data;
import com.nelyubin.improvetest.savedata.DataOnPreferences;

public final class ViewButtonClickListener implements View.OnClickListener
{
    private static final String DATA_ENTERED_INCORRECTLY = "Data entered incorrectly";
    private static final String PHOTO_IS_NOT_ADDED = "Photo is not added";

    private TextInputEditText emailTextInputEditText;
    private TextInputEditText phoneTextInputEditText;
    private TextInputEditText passwordTextInputEditText;

    public ViewButtonClickListener(TextInputEditText emailTextInputEditText,
                                   TextInputEditText phoneTextInputEditText, TextInputEditText passwordTextInputEditText)
    {
        this.emailTextInputEditText = emailTextInputEditText;
        this.phoneTextInputEditText = phoneTextInputEditText;
        this.passwordTextInputEditText = passwordTextInputEditText;
    }

    @Override
    public void onClick(View view)
    {
        if(emailTextInputEditText.getText().toString().isEmpty()
                || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailTextInputEditText.getText().toString()).matches()
                || phoneTextInputEditText.getText().toString().isEmpty()
                || !phoneTextInputEditText.getText().toString().matches("\\++?7?\\d{10}")
                || !(12==phoneTextInputEditText.getText().toString().length())
                || passwordTextInputEditText.getText().toString().isEmpty()
                || passwordTextInputEditText.getText().toString().length() < 6
                || !passwordTextInputEditText.getText().toString().matches(".*[A-Za-z]+.*")
                || !passwordTextInputEditText.getText().toString().matches(".*\\d+.*")
                || passwordTextInputEditText.getText().toString().matches(".*\\W+.*"))
        {
            Toast.makeText(view.getContext(), DATA_ENTERED_INCORRECTLY, Toast.LENGTH_LONG).show();
        }
        else if(!DataOnPreferences.isPhotoAdd((Activity) view.getContext()))
        {
            Toast.makeText(view.getContext(), PHOTO_IS_NOT_ADDED, Toast.LENGTH_LONG).show();
        }
        else
        {
            final Data dataForSend = new Data();
            dataForSend.setEmailAddress(emailTextInputEditText.getText().toString());
            dataForSend.setPhoneNumber(phoneTextInputEditText.getText().toString());
            dataForSend.setPassword(passwordTextInputEditText.getText().toString());
            DataOnPreferences.saveDataOnPreferences((Activity) view.getContext(), dataForSend);
            final Intent startSendDataActivity = new Intent(view.getContext(), SendDataActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            view.getContext().startActivity(startSendDataActivity);
        }
    }
}

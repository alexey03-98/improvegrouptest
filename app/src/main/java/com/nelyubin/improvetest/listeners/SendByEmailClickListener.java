package com.nelyubin.improvetest.listeners;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.nelyubin.improvetest.MainActivity;
import com.nelyubin.improvetest.R;
import com.nelyubin.improvetest.model.Data;
import com.nelyubin.improvetest.savedata.DataOnPreferences;

public final class SendByEmailClickListener implements View.OnClickListener
{
    @Override
    public void onClick(View view)
    {
        final Intent sendByEmail = new Intent(Intent.ACTION_SEND);
        final Data dataForSend = DataOnPreferences.getDataFromPreferences((Activity) view.getContext());
        sendByEmail.putExtra(Intent.EXTRA_SUBJECT, view.getContext().getString(R.string.app_name)
                + ":" + view.getContext().getString(R.string.string_data_questionnaire));
        sendByEmail.putExtra(Intent.EXTRA_TEXT, view.getContext().getString(R.string.string_email) + ":" + dataForSend.getEmailAddress() + "\n\r"
                + view.getContext().getString(R.string.string_phone) + ":" + dataForSend.getPhoneNumber() + "\n\r"
                + view.getContext().getString(R.string.string_password) + ":" + dataForSend.getPassword() + "\n\r");
        sendByEmail.putExtra(Intent.EXTRA_STREAM, Uri.parse(dataForSend.getPhotoURI()));
        sendByEmail.setType("message/rfc822");
        DataOnPreferences.deleteDataFromPreferences((Activity) view.getContext());
        view.getContext().startActivity(new Intent(view.getContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        view.getContext().startActivity(Intent.createChooser(sendByEmail, view.getContext().getString(R.string.string_choose_email_client)));
    }
}

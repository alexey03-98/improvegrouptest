package com.nelyubin.improvetest;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.nelyubin.improvetest.activityscontent.ContentOfMainActivity;
import com.nelyubin.improvetest.savedata.DataOnPreferences;
import com.nelyubin.improvetest.workwithphoto.CircularTransformation;
import com.nelyubin.improvetest.workwithphoto.GeneratorURI;
import com.squareup.picasso.Picasso;

public final class MainActivity extends AppCompatActivity
{
    final int REQUEST_CODE_PHOTO = 121;
    final int REQUEST_CODE_READ_EXTERNAL_STORAGE = 137;
    String photoUri;
    ImageButton imageButtonAddPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ContentOfMainActivity contentOfMainActivity = new ContentOfMainActivity(this);
        contentOfMainActivity.initializationContentActivity();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_READ_EXTERNAL_STORAGE);
        }
        imageButtonAddPhoto = findViewById(R.id.imageButtonAddPhoto);
        imageButtonAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoUri = GeneratorURI.generateUri().toString();
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.parse(photoUri));
                startActivityForResult(cameraIntent, REQUEST_CODE_PHOTO);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_PHOTO)
        {
            if(resultCode == RESULT_OK)
            {
                DataOnPreferences.savePhotoOnReferences(this, true, photoUri);
                Picasso.with(this)
                        .load(Uri.parse(photoUri)).fit().transform(new CircularTransformation())
                        .placeholder(R.drawable.ic_add_photo).fit()
                        .error(R.drawable.ic_add_photo).fit()
                        .into(imageButtonAddPhoto);

            }
            else
            {
                DataOnPreferences.savePhotoOnReferences(this, false,"");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case REQUEST_CODE_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(this, getString(R.string.string_need_to_set_permission), Toast.LENGTH_LONG).show();
                }
        }
    }
}

package com.nelyubin.improvetest.activityscontent;

import android.app.Activity;
import android.net.Uri;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.ImageButton;

import com.nelyubin.improvetest.R;
import com.nelyubin.improvetest.errorhandling.EmailInputErrors;
import com.nelyubin.improvetest.errorhandling.PasswordInputErrors;
import com.nelyubin.improvetest.errorhandling.PhoneInputErrors;
import com.nelyubin.improvetest.listeners.ViewButtonClickListener;
import com.nelyubin.improvetest.model.Data;
import com.nelyubin.improvetest.savedata.DataOnPreferences;
import com.nelyubin.improvetest.workwithphoto.CircularTransformation;
import com.squareup.picasso.Picasso;

public final class ContentOfMainActivity
{
    private Activity activityMainActivity;

    public ContentOfMainActivity(final Activity activityMainActivity)
    {
        this.activityMainActivity = activityMainActivity;
    }

    public void initializationContentActivity()
    {
        final Data dataForSend = DataOnPreferences.getDataFromPreferences(activityMainActivity);
        final TextInputLayout emailInputTextLayout = activityMainActivity.findViewById(R.id.textInputLayoutEmail);
        final TextInputLayout phoneInputTextLayout = activityMainActivity.findViewById(R.id.textInputLayoutPhone);
        final TextInputLayout passwordInputTextLayout = activityMainActivity.findViewById(R.id.textInputLayoutPassword);
        final TextInputEditText emailTextInputEditText = activityMainActivity.findViewById(R.id.textInputEditTextEmail);
        final TextInputEditText phoneTextInputEditText = activityMainActivity.findViewById(R.id.textInputEditTextPhone);
        final TextInputEditText passwordTextInputEditText = activityMainActivity.findViewById(R.id.textInputEditTextPassword);
        final Button viewStartSendActivityButton = activityMainActivity.findViewById(R.id.buttonViewStartNextActivity);
        final ImageButton imageButtonAddPhoto = activityMainActivity.findViewById(R.id.imageButtonAddPhoto);
        emailTextInputEditText.setText(dataForSend.getEmailAddress());
        phoneTextInputEditText.setText(dataForSend.getPhoneNumber());
        passwordTextInputEditText.setText(dataForSend.getPassword());
        Picasso.with(activityMainActivity)
                .load(Uri.parse(dataForSend.getPhotoURI())).fit().transform(new CircularTransformation())
                .placeholder(R.drawable.ic_add_photo).fit()
                .error(R.drawable.ic_add_photo).fit()
                .into(imageButtonAddPhoto);
        emailTextInputEditText.setOnFocusChangeListener(
                new EmailInputErrors(emailInputTextLayout,emailTextInputEditText));
        phoneTextInputEditText.setOnFocusChangeListener(
                new PhoneInputErrors(phoneInputTextLayout,phoneTextInputEditText));
        passwordTextInputEditText.setOnFocusChangeListener(
                new PasswordInputErrors(passwordInputTextLayout,passwordTextInputEditText));
        viewStartSendActivityButton.setOnClickListener(new ViewButtonClickListener(emailTextInputEditText,
                phoneTextInputEditText, passwordTextInputEditText));
    }
}

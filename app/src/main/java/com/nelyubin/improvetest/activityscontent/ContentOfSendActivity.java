package com.nelyubin.improvetest.activityscontent;

import android.app.Activity;
import android.net.Uri;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nelyubin.improvetest.R;
import com.nelyubin.improvetest.listeners.SendByEmailClickListener;
import com.nelyubin.improvetest.model.Data;
import com.nelyubin.improvetest.savedata.DataOnPreferences;
import com.squareup.picasso.Picasso;

public final class ContentOfSendActivity
{
    private Activity sendActivity;

    public ContentOfSendActivity(Activity sendActivity)
    {
        this.sendActivity = sendActivity;
    }

    public void initializationContentActivity()
    {
        final Data dataForSend = DataOnPreferences.getDataFromPreferences(sendActivity);
        final TextView textViewWithEmailAddress = sendActivity.findViewById(R.id.textViewWithEmailAddress);
        final TextView textViewWithPhoneNumber = sendActivity.findViewById(R.id.textViewWithPhoneNumber);
        final TextView textViewWithPassword = sendActivity.findViewById(R.id.textViewWithPassword);
        final ImageView imageViewWithPhotoForSend  = sendActivity.findViewById(R.id.imageViewWithPhotoForSend);
        final Button buttonSendByEmail = sendActivity.findViewById(R.id.buttonSendByEmail);
        textViewWithEmailAddress.setText(dataForSend.getEmailAddress());
        textViewWithPhoneNumber.setText(changePhoneFormat(dataForSend.getPhoneNumber()));
        textViewWithPassword.setText(dataForSend.getPassword());
        Picasso.with(sendActivity)
                .load(Uri.parse(dataForSend.getPhotoURI())).fit()
                .placeholder(R.drawable.place_holder_for_imageview_with_photo).fit()
                .error(R.drawable.place_holder_for_imageview_with_photo).fit()
                .into(imageViewWithPhotoForSend);
        buttonSendByEmail.setOnClickListener(new SendByEmailClickListener());
    }

    private String changePhoneFormat(final String twentyDigitPhoneNumber)
    {
        String changedPhoneNumber = new StringBuffer(twentyDigitPhoneNumber.substring(0,2))
                .append(" " + "(" +twentyDigitPhoneNumber.substring(2,5) + ") "
                        + twentyDigitPhoneNumber.substring(5,8) + " "
                        + twentyDigitPhoneNumber.substring(8,10) + " "
                        + twentyDigitPhoneNumber.substring(10,12)).toString();
        return changedPhoneNumber;
    }
}

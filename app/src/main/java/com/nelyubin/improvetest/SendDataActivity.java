package com.nelyubin.improvetest;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import com.nelyubin.improvetest.activityscontent.ContentOfSendActivity;

public final class SendDataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_data);
        final ActionBar actionBarWithHomeButton = getSupportActionBar();
        actionBarWithHomeButton.setHomeButtonEnabled(true);
        actionBarWithHomeButton.setDisplayHomeAsUpEnabled(true);
        final ContentOfSendActivity contentOfSendActivity = new ContentOfSendActivity(this);
        contentOfSendActivity.initializationContentActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home:
                final Intent startMainActivity = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(startMainActivity);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.nelyubin.improvetest.model;


public final class Data
{
    private String photoURI;
    private String emailAddress;
    private String phoneNumber;
    private String password;

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public String getPhoneNumber() {return phoneNumber;}

    public String getPhotoURI() {
        return photoURI;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPhotoURI(String photoURI) {
        this.photoURI = photoURI;
    }
}

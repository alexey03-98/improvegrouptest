package com.nelyubin.improvetest.savedata;

import android.app.Activity;
import android.content.SharedPreferences;
import com.nelyubin.improvetest.model.Data;

public final class DataOnPreferences
{
    private final static String KEY_DATA_PREFERENCES = "DataPReferences";
    private final static String KEY_IS_PHOTO_ADD = "IsPhotoAdd";
    private final static String KEY_EMAIL_ADDRESS = "EmailAddress";
    private final static String KEY_PHONE_NUMBER = "PhoneNumber";
    private final static String KEY_PASSWORD = "Password";
    private final static String KEY_PHOTO_URI = "PhotoUri";

    public static void savePhotoOnReferences(Activity activity, boolean isPhotoAdd, String PhotoUri)
    {
        final SharedPreferences sharedPreferences = activity.getSharedPreferences(KEY_DATA_PREFERENCES,Activity.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_PHOTO_URI, PhotoUri).apply();
        sharedPreferences.edit().putBoolean(KEY_IS_PHOTO_ADD, isPhotoAdd).apply();
    }

    public static boolean isPhotoAdd(Activity activity)
    {
        return activity.getSharedPreferences(KEY_DATA_PREFERENCES,Activity.MODE_PRIVATE)
                .getBoolean(KEY_IS_PHOTO_ADD, false);
    }

    public static void saveDataOnPreferences(Activity activity, Data dataForSend)
    {
        final SharedPreferences sharedPreferences = activity.getSharedPreferences(KEY_DATA_PREFERENCES, Activity.MODE_PRIVATE);
        sharedPreferences.edit().putString(KEY_EMAIL_ADDRESS, dataForSend.getEmailAddress()).apply();
        sharedPreferences.edit().putString(KEY_PHONE_NUMBER, dataForSend.getPhoneNumber()).apply();
        sharedPreferences.edit().putString(KEY_PASSWORD, dataForSend.getPassword()).apply();
    }

    public static Data getDataFromPreferences(Activity activity)
    {
        final Data dataForSend = new Data();
        SharedPreferences sharedPreferences = activity.getSharedPreferences(KEY_DATA_PREFERENCES, Activity.MODE_PRIVATE);
        dataForSend.setPhotoURI(sharedPreferences.getString(KEY_PHOTO_URI,""));
        dataForSend.setEmailAddress(sharedPreferences.getString(KEY_EMAIL_ADDRESS, ""));
        dataForSend.setPhoneNumber(sharedPreferences.getString(KEY_PHONE_NUMBER,""));
        dataForSend.setPassword(sharedPreferences.getString(KEY_PASSWORD,""));
        return dataForSend;
    }

    public static void deleteDataFromPreferences(Activity activity)
    {
        final SharedPreferences sharedPreferences = activity.getSharedPreferences(KEY_DATA_PREFERENCES, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().apply();
    }
}
